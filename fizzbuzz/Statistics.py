from collections import Counter


class KeyRequest:
    def __init__(self, int1: int, str1: str, int2: int, str2: str, limit: int):
        self.int1 = int1
        self.str1 = str1
        self.int2 = int2
        self.str2 = str2
        self.limit = limit
        self.key_as_string = str(self.int1) + self.str1 + str(self.int2) + self.str2 + str(self.limit)

    def __hash__(self):
        return self.key_as_string.__hash__()

    def __eq__(self, other):
        return self.key_as_string.__eq__(other.key_as_string)


class StatisticsRecorder:

    def record(self, int1: int, str1: str, int2: int, str2: str, limit: int):
        pass

    def most_called(self):
        pass


class StatisticsRecorderThreadSafe(StatisticsRecorder):

    def __init__(self):
        self.requests = Counter()

    def record(self, int1: int, str1: str, int2: int, str2: str, limit: int):
        key = KeyRequest(int1, str1, int2, str2, limit)
        self.requests[key] += 1

    def most_called(self):
        pairs = self.requests.most_common(2)
        key1 = pairs[0][0]
        value1 = pairs[0][1]

        return {'parameters': {
            'int1': key1.int1,
            'str1': key1.str1,
            'int2': key1.int2,
            'str2': key1.str2,
            'limit': key1.limit,
        }, 'occurrences': {
            'number': value1
        },
        }

#TODO : if needed, wirte a StatisticRecorderMultiProcessSafe using Flask-caching configured with File, or a database