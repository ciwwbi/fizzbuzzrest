import os

from flask import Flask


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    from fizzbuzz.controller import GeneratorController, StatisticsController
    app.register_blueprint(GeneratorController.blueprint)
    app.register_blueprint(StatisticsController.blueprint)

    return app
