VERY_BIG_INT = 2^15
UNDEFINED_STRING = "undefined"


def list_of_answers(int1: int, str1: str, int2: int, str2: str, limit: int) -> list :
    trans = StringTransformer(int1, str1, int2, str2)

    return [trans.to_string(number) for number in range(1, limit+1)]


def is_divisible_by(dividend: int, divisor: int) -> bool:
    return (dividend % divisor) == 0


def is_not_divisible(result_string: str) -> bool:
    return not result_string


class StringTransformer:
    divisor1 = VERY_BIG_INT
    string1 = UNDEFINED_STRING

    divisor2 = VERY_BIG_INT
    string2 = UNDEFINED_STRING

    def __init__(self,
                 divisor1: int = VERY_BIG_INT,
                 string1: str = UNDEFINED_STRING,
                 divisor2: int = VERY_BIG_INT,
                 string2: str = UNDEFINED_STRING):
        self.divisor1 = divisor1
        self.string1  = string1
        self.divisor2 = divisor2
        self.string2  = string2

    def to_string(self, number: int):
        result_string = ''

        if is_divisible_by(number, self.divisor1):
            result_string += self.string1

        if is_divisible_by(number, self.divisor2):
            result_string += self.string2

        if is_not_divisible(result_string):
            result_string = str(number)

        return result_string
