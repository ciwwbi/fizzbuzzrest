from flask import Blueprint, request, abort, escape
from fizzbuzz.Generator import list_of_answers

from fizzbuzz.controller import stat_recorder

blueprint = Blueprint('generator', __name__, url_prefix='/fizzbuzz')

PARAM_INT1 = 'int1'
PARAM_INT2 = 'int2'
PARAM_LIMIT = 'limit'

ERROR_TYPE_INT = "{} should be an integer"


@blueprint.route("/answer")
def generate():
    divisor1 = request.args.get(PARAM_INT1, type=int)
    if not divisor1:
        abort(400, ERROR_TYPE_INT.format(PARAM_INT1))

    string1 = escape(request.args.get('str1', type=str))

    divisor2 = request.args.get(PARAM_INT2, type=int)
    if not divisor2:
        abort(400, ERROR_TYPE_INT.format(PARAM_INT2))

    string2 = escape(request.args.get('str2', type=str))

    limit = request.args.get(PARAM_LIMIT, type=int)
    if not limit:
        abort(400, ERROR_TYPE_INT.format(PARAM_LIMIT))

    stat_recorder.record(divisor1, string1, divisor2, string2, limit)

    return {"data": list_of_answers(divisor1, string1, divisor2, string2, limit)}

