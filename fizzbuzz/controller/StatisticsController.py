from flask import Blueprint

from fizzbuzz.controller import stat_recorder

blueprint = Blueprint('statistics', __name__, url_prefix='/fizzbuzz')


@blueprint.route("/statistics")
def most_seen():
    most_called_request = stat_recorder.most_called()
    return {"data": most_called_request}
