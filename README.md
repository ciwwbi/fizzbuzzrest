# Exercise statement : Write a simple fizz-buzz REST server. 

The original fizz-buzz consists in writing all numbers from 1 to 100, and just replacing all multiples of 3 by "fizz", all multiples of 5 by "buzz", and all multiples of 15 by "fizzbuzz".

The output would look like this: "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,...".

 

Your goal is to implement a web server that will expose a REST API endpoint that : 

1. Accepts five parameters : three integers int1, int2 and limit, and two strings str1 and str2.
2. Returns a list of strings with numbers from 1 to limit, where: all multiples of int1 are replaced by str1, all multiples of int2 are replaced by str2, all multiples of int1 and int2 are replaced by str1str2.
3. The server needs to be:
   * Ready for production
   * Easy to maintain by other developers
4. Add a statistics endpoint allowing users to know what the most frequent request has been. This endpoint should:
   - Accept no parameter
   - Return the parameters corresponding to the most used request, as well as the number of hits for this request"

# Production Architecture
This application uses Flask development server, that is not production-proof.

Use a WSGI server (gunicorn, uWSGI) : source code is exactly the same, only the software environment is different. 

# How to configure environment
* Check that python 3 is installed or install it
* Check that pip is installed or install it
* Go to application's root path and activate virtual environment
```shell
$ cd <application_path>
$ source venv/bin/activate
```
* Install packages with pip and requirements.txt
```shell
$ pip install -r requirements.txt
```

# How to launch development server
* Go to application's root path and launch script run_serveur.sh
```shell
$ cd <application_path>
$ ./run_serveur.sh
 * Serving Flask app 'fizzbuzz' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)
```
Application is now accessible at :
* http://127.0.0.1:5000/fizzbuzz/answer?int1=<integer>&str1=<string>&int2=<integer>&str2=<string>&limit=<integer>

## Example
### Webservice REST Fizzbuzz
http://127.0.0.1:5000/fizzbuzz/answer?int1=3&str1=fizz&int2=5&str2=buzz&limit=16
```
{
  "data": [
    "1",
    "2",
    "fizz",
    "4",
    "buzz",
    "fizz",
    "7",
    "8",
    "fizz",
    "buzz",
    "11",
    "fizz",
    "13",
    "14",
    "fizzbuzz",
    "16"
  ]
}
```

### Webservice REST statistics
http://127.0.0.1:5000/fizzbuzz/statistics
```
{
  "data": {
    "occurrences": {
      "number": 1
    },
    "parameters": {
      "int1": 3,
      "int2": 5,
      "limit": 31,
      "str1": "fizz",
      "str2": "buzz"
    }
  }
}
```

# How to test application
Go to application's root path and run pytest :
```shell
$ cd <application_path>
$ pytest tests
```

To run only some tests :
```shell
# Integration tests
$ pytest tests/integ

# Unit tests
$ pytest tests/units
```

# How to calculate coverage
Go to application's root path and run pytest with coverage option :
```shell
$ cd <application_path>
$ pytest --cov=fizzbuzz tests
```

To have more information (as seing lines covered or not), you should generate html report :
```shell
$ pytest --cov=fizzbuzz tests --cov-report html
$ firefox ./htmlcov/index.html
```
 
