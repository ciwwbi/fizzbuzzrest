def test_giving_2_as_limit_returns_success(client):
    response_fizzbuzz = client.get("/fizzbuzz/answer?int1=7&str1=dum&int2=5&str2=my&limit=2")

    assert 200 == response_fizzbuzz.status_code
    assert ["1", "2"] == response_fizzbuzz.json["data"]


def test_giving_five_correct_parameters_returns_success(client):
    response_fizzbuzz = client.get("/fizzbuzz/answer?int1=2&str1=doubi&int2=3&str2=dou&limit=6")

    assert 200 == response_fizzbuzz.status_code
    assert ["1", "doubi", "dou", "doubi", "5", "doubidou"] == response_fizzbuzz.json["data"]


def test_giving_incorrect_int1_returns_error_400(client):
    response_fizzbuzz = client.get("/fizzbuzz/answer?int1=incorrect&str1=doubi&int2=3&str2=dou&limit=6")

    assert 400 == response_fizzbuzz.status_code
    assert b'int1' in response_fizzbuzz.data
    assert b'integer' in response_fizzbuzz.data


def test_giving_incorrect_int2_returns_error_400(client):
    response_fizzbuzz = client.get("/fizzbuzz/answer?int1=2&str1=doubi&int2=incorrect&str2=dou&limit=6")

    assert 400 == response_fizzbuzz.status_code
    assert b'int2' in response_fizzbuzz.data
    assert b'integer' in response_fizzbuzz.data


def test_giving_incorrect_limit_returns_error_400(client):
    response_fizzbuzz = client.get("/fizzbuzz/answer?int1=2&str1=doubi&int2=3&str2=dou&limit=;!")

    assert 400 == response_fizzbuzz.status_code
    assert b'limit' in response_fizzbuzz.data
    assert b'integer' in response_fizzbuzz.data


def test_giving_unusual_str1_returns_success(client):
    response_fizzbuzz = client.get("/fizzbuzz/answer?int1=2&str1=!;%20&int2=3&str2=dou&limit=6")

    assert 200 == response_fizzbuzz.status_code
    assert ["1", "!; ", "dou", "!; ", "5", "!; dou"] == response_fizzbuzz.json["data"]


def test_giving_unusual_str2_returns_success(client):
    response_fizzbuzz = client.get("/fizzbuzz/answer?int1=2&str1=doubi&int2=3&str2=-%23&limit=6")

    assert 200 == response_fizzbuzz.status_code
    assert ["1", "doubi", "-#", "doubi", "5", "doubi-#"] == response_fizzbuzz.json["data"]
