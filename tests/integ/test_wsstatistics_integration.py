import pytest


@pytest.fixture
def reinit_stats():
    from fizzbuzz.controller import stat_recorder
    stat_recorder.requests.clear()


def given_calling_fizzbuzz_request1(client):
    client.get("/fizzbuzz/answer?int1=2&str1=doubi&int2=3&str2=dou&limit=6")


def given_calling_fizzbuzz_request2(client):
    client.get("/fizzbuzz/answer?int1=3&str1=fizz&int2=5&str2=buzz&limit=10")


def test_giving_requests_returns_the_most_called(client, reinit_stats):
    given_calling_fizzbuzz_request1(client)
    given_calling_fizzbuzz_request2(client)
    given_calling_fizzbuzz_request1(client)

    response_stats = client.get("/fizzbuzz/statistics")

    assert 200 == response_stats.status_code
    expected_parameters = {'int1': 2, 'str1': 'doubi', 'int2': 3, 'str2': 'dou', 'limit': 6}
    assert expected_parameters == response_stats.json["data"]["parameters"]
    assert {'number': 2} == response_stats.json["data"]["occurrences"]


def test_giving_other_requests_returns_the_most_called(client, reinit_stats):
    given_calling_fizzbuzz_request1(client)
    given_calling_fizzbuzz_request2(client)
    given_calling_fizzbuzz_request2(client)
    given_calling_fizzbuzz_request2(client)

    response_stats = client.get("/fizzbuzz/statistics")

    assert 200 == response_stats.status_code
    assert {'number': 3} == response_stats.json["data"]["occurrences"]
    expected_parameters = {'int1': 3, 'str1': 'fizz', 'int2': 5, 'str2': 'buzz', 'limit': 10}
    assert expected_parameters == response_stats.json["data"]["parameters"]
