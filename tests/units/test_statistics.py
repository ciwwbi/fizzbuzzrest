from fizzbuzz.Statistics import StatisticsRecorderThreadSafe


def test_giving_one_request_returns_this_request_with_one_occurrence():

    stats = StatisticsRecorderThreadSafe()
    stats.record(3, 'fizz', 5, 'buzz', 1)

    effective_stat = stats.most_called()
    expected_stat = {
        'parameters': {
            'int1': 3,
            'str1': 'fizz',
            'int2': 5,
            'str2': 'buzz',
            'limit': 1,
        },
        'occurrences': {
            'number': 1
        }
    }

    assert expected_stat == effective_stat


def test_giving_two_requests_returns_the_one_with_the_bigger_occurrence():

    given_request_1 = (3, 'fizz', 5, 'buzz', 1)
    given_request_2 = (7, 'dadou', 11, 'ronron', 78)

    stats = StatisticsRecorderThreadSafe()
    stats.record(*given_request_2)
    stats.record(*given_request_2)
    stats.record(*given_request_2)

    stats.record(*given_request_1)

    effective_stat = stats.most_called()

    expected_stat = {
        'parameters': {
            'int1': 7,
            'str1': 'dadou',
            'int2': 11,
            'str2': 'ronron',
            'limit': 78,
        },
        'occurrences': {
            'number': 3
        }
    }

    assert expected_stat == effective_stat
