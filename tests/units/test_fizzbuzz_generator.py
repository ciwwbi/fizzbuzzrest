from fizzbuzz.Generator import list_of_answers

DUMMY_INT1 = 12456
DUMMY_STR1 = "fizz"

DUMMY_INT2 = 64785
DUMMY_STR2 = "buzz"


def test_giving_limit_1_generates_1():
    assert ["1"] == list_of_answers(DUMMY_INT1, DUMMY_STR1, DUMMY_INT2, DUMMY_STR2, 1)


def test_giving_limit_2_generates_1_2():
    assert ["1", "2"] == list_of_answers(DUMMY_INT1, DUMMY_STR1, DUMMY_INT2, DUMMY_STR2, 2)


def test_giving_limit_4_int1_2_str1_toto_generates_1_toto_3_toto():
    assert ["1", "toto", "3", "toto"] == list_of_answers(2, "toto", DUMMY_INT2, DUMMY_STR2, 4)


def test_giving_limit_7_int1_3_str1_three_int2_2_str2_two_generates_1_two_three_two_5_threetwo_7():
    assert ["1", "two", "three", "two", "5", "threetwo", "7"] == list_of_answers(3, "three", 2, "two", 7)
